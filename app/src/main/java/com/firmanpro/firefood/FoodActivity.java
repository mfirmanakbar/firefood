package com.firmanpro.firefood;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FoodActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private DatabaseReference tb_food;
    private SwipeRefreshLayout swipe_refresh;
    private RecyclerView rv_food;
    private RecyclerView.LayoutManager lm_food;
    private FoodAdapter foodAdapter;
    private List<FoodEntity> foodList = new ArrayList<FoodEntity>();
    public String category_id, category_name;
    private TextView txtCategoryHeader;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        txtCategoryHeader = (TextView)findViewById(R.id.txtCategoryHeader);

        Intent intent = getIntent();
        category_id = intent.getStringExtra("category_id");
        category_name = intent.getStringExtra("category_name");

        txtCategoryHeader.setText(category_name);

        tb_food = FirebaseDatabase.getInstance().getReference("tb_food").child(category_id);

        swipe_refresh = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        /**untuk menjalankan onRefresh()*/
        swipe_refresh.setOnRefreshListener(this);

        rv_food = (RecyclerView)findViewById(R.id.rv_food);
        lm_food = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_food.setLayoutManager(lm_food);
        rv_food.setItemAnimator(new DefaultItemAnimator());
        foodAdapter = new FoodAdapter(getApplicationContext(), foodList, FoodActivity.this);
        rv_food.setAdapter(foodAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        swipe_refresh.setRefreshing(true);
        tb_food.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                foodList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    FoodEntity data = ds.getValue(FoodEntity.class);
                    foodList.add(data);
                }
                foodAdapter.notifyDataSetChanged();
                /**loading close*/
                swipe_refresh.setRefreshing(false);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                swipe_refresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        onStart();
    }

    public void popupCrudFood(final String idFood, final String nameFood, final Activity activity, final String idCaregory) {
        String titleDialog = "";

        if (idFood==null)
            titleDialog = "Create Food";
        else
            titleDialog = "Update Food";

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater)activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_food, null);
        builder.setTitle(titleDialog);
        builder.setView(dialogView);

        final android.app.AlertDialog alertDialog = builder.create();

        final EditText txtPopupFoodName = (EditText) dialogView.findViewById(R.id.txtPopupFoodName);
        final Button btnPopupFoodSOU = (Button) dialogView.findViewById(R.id.btnPopupFoodSOU);
        final Button btnPopupFoodDelete = (Button) dialogView.findViewById(R.id.btnPopupFoodDelete);
        final Button btnPopupFoodClose = (Button) dialogView.findViewById(R.id.btnPopupFoodClose);

        if (idFood==null){
            btnPopupFoodSOU.setText("Save");
            btnPopupFoodDelete.setVisibility(View.GONE);
        }else {
            btnPopupFoodSOU.setText("Update");
            btnPopupFoodDelete.setVisibility(View.VISIBLE);
            txtPopupFoodName.setText(nameFood);
        }

        btnPopupFoodClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnPopupFoodSOU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idFood==null){
                    //save
                    String id = tb_food.push().getKey();
                    FoodEntity fe = new FoodEntity();
                    fe.setFoodID(id);
                    fe.setFoodName(txtPopupFoodName.getText().toString());
                    fe.setCategoryID(category_id);
                    tb_food.child(id).setValue(fe);
                    alertDialog.dismiss();
                }else {
                    //update
                    Log.d("tb_foods1",idCaregory);
                    Log.d("tb_foods2",idFood);
                    FoodEntity fe = new FoodEntity();
                    fe.setFoodID(idFood);
                    fe.setFoodName(txtPopupFoodName.getText().toString());
                    fe.setCategoryID(idCaregory);
                    tb_food = FirebaseDatabase.getInstance().getReference("tb_food").child(idCaregory);
                    tb_food.child(idFood).setValue(fe);
                    alertDialog.dismiss();
                }
            }
        });

        btnPopupFoodDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("tb_foods1",idCaregory);
                Log.d("tb_foods2",idFood);
                tb_food = FirebaseDatabase.getInstance().getReference("tb_food").child(idCaregory);
                tb_food.child(idFood).removeValue();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add:
                popupCrudFood(null, null, FoodActivity.this, null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
