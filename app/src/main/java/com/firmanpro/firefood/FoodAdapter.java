package com.firmanpro.firefood;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by firmanmac on 10/15/17.
 *  Urutan mudah membuat adapter:
 *  1. MyViewHolder
 *  2. extend RecyclerView.Adapter<FoodAdapter.MyViewHolder>
 *  3. implement method
 *  4. private Context context;
 *  5. List<FoodEntity> foodEntities;
 *  6. buat constractor
 *  7. lengkapi onCreateViewHolder
 *  8. lengkapi getItemCount
 *  9. lengkapi onBindViewHolder
 */

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.MyViewHolder> {

    private Context context;
    private List<FoodEntity> foodEntities;
    private Activity activity;

    public FoodAdapter(Context context, List<FoodEntity> foodEntities, Activity activity) {
        this.context = context;
        this.foodEntities = foodEntities;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_food, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final FoodEntity data = foodEntities.get(position);
        holder.txtFoodName.setText(data.getFoodName());

        holder.cv_item_food.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                FoodActivity ma = new FoodActivity();
                ma.popupCrudFood(data.getFoodID(), data.getFoodName(), activity, data.getCategoryID());
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return foodEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtFoodName;
        protected CardView cv_item_food;

        public MyViewHolder (View view) {
            super(view);
            txtFoodName = (TextView)view.findViewById(R.id.txtFoodName);
            cv_item_food = (CardView) view.findViewById(R.id.cv_item_food);
        }
    }



}
