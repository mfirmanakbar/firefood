package com.firmanpro.firefood;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private DatabaseReference tb_category;
    private SwipeRefreshLayout swipe_refresh;
    private RecyclerView rv_category;
    private RecyclerView.LayoutManager lm_category;
    private CategoryAdapter categoryAdapter;
    private List<CategoryEntity> categoryList = new ArrayList<CategoryEntity>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        /**Firebase*/
        tb_category = FirebaseDatabase.getInstance().getReference("tb_category");

        swipe_refresh = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh);
        /**untuk menjalankan onRefresh()*/
        swipe_refresh.setOnRefreshListener(this);

        /**RecyclerView*/
        rv_category = (RecyclerView)findViewById(R.id.rv_category);
        lm_category = new GridLayoutManager(this, 1); /**kolom tampilan item*/
        rv_category.setLayoutManager(lm_category);
        rv_category.setItemAnimator(new DefaultItemAnimator());
        categoryAdapter = new CategoryAdapter(getApplicationContext(), categoryList, CategoryActivity.this);
        rv_category.setAdapter(categoryAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();

        /**loading show*/
        swipe_refresh.setRefreshing(true);
        tb_category.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                categoryList.clear();
                for (DataSnapshot dS : dataSnapshot.getChildren()){
                    CategoryEntity data = dS.getValue(CategoryEntity.class);
                    categoryList.add(data);
                }
                categoryAdapter.notifyDataSetChanged();
                /**loading close*/
                swipe_refresh.setRefreshing(false);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                swipe_refresh.setRefreshing(false);
            }
        });

    }

    public void popupCrudCategory(final String idCategory, final String nameCategory, final Activity activity) {
        String titleDialog = "";

        if (idCategory==null)
            titleDialog = "Create Category";
        else
            titleDialog = "Update Category";

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        final LayoutInflater inflater = (LayoutInflater)activity.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final View dialogView = inflater.inflate(R.layout.popup_category, null);
        builder.setTitle(titleDialog);
        builder.setView(dialogView);

        final android.app.AlertDialog alertDialog = builder.create();

        final EditText txtPopupCategoryName = (EditText) dialogView.findViewById(R.id.txtPopupCategoryName);
        final Button btnPopupCategorySOU = (Button) dialogView.findViewById(R.id.btnPopupCategorySOU);
        final Button btnPopupCategoryDelete = (Button) dialogView.findViewById(R.id.btnPopupCategoryDelete);
        final Button btnPopupCategoryClose = (Button) dialogView.findViewById(R.id.btnPopupCategoryClose);

        if (idCategory==null){
            btnPopupCategorySOU.setText("Save");
            btnPopupCategoryDelete.setVisibility(View.GONE);
        }else {
            btnPopupCategorySOU.setText("Update");
            btnPopupCategoryDelete.setVisibility(View.VISIBLE);
            txtPopupCategoryName.setText(nameCategory);
        }

        btnPopupCategoryClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnPopupCategorySOU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idCategory==null){
                    /**save*/
                    String id = tb_category.push().getKey();
                    CategoryEntity ce = new CategoryEntity();
                    ce.setCategoryID(id);
                    ce.setCategoryName(txtPopupCategoryName.getText().toString());
                    tb_category.child(id).setValue(ce);
                    alertDialog.dismiss();
                }else {
                    /**update*/
                    DatabaseReference tb_category_update = FirebaseDatabase.getInstance().getReference("tb_category").child(idCategory);
                    CategoryEntity ce = new CategoryEntity();
                    ce.setCategoryID(idCategory);
                    ce.setCategoryName(txtPopupCategoryName.getText().toString());
                    tb_category_update.setValue(ce);
                    alertDialog.dismiss();
                }
            }
        });

        btnPopupCategoryDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference tb_category_del = FirebaseDatabase.getInstance().getReference("tb_category").child(idCategory);
                DatabaseReference tb_food = FirebaseDatabase.getInstance().getReference("tb_food").child(idCategory);
                tb_category_del.removeValue();
                tb_food.removeValue();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onRefresh() {
        onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_add:
                popupCrudCategory(null, null, CategoryActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
