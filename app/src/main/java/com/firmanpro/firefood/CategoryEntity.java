package com.firmanpro.firefood;

/**
 * Created by firmanmac on 10/15/17.
 */

public class CategoryEntity {

    public String categoryID, categoryName;

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
