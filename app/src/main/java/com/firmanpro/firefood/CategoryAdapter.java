package com.firmanpro.firefood;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by firmanmac on 10/15/17.
 *  Urutan mudah membuat adapter:
 *  1. MyViewHolder
 *  2. extend RecyclerView.Adapter<CategoryAdapter.MyViewHolder>
 *  3. implement method
 *  4. private Context context;
 *  5. List<CategoryEntity> categoryEntities;
 *  6. buat constractor
 *  7. lengkapi onCreateViewHolder
 *  8. lengkapi getItemCount
 *  9. lengkapi onBindViewHolder
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context context;
    private List<CategoryEntity> categoryEntities;
    private Activity activity;

    public CategoryAdapter(Context context, List<CategoryEntity> categoryEntities, Activity activity) {
        this.context = context;
        this.categoryEntities = categoryEntities;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CategoryEntity data = categoryEntities.get(position);
        holder.txtCategoryName.setText(data.getCategoryName());

        holder.cv_item_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent itn = new Intent(activity, FoodActivity.class);
                itn.putExtra("category_id", data.getCategoryID());
                itn.putExtra("category_name", data.getCategoryName());
                activity.startActivity(itn);
            }
        });

        holder.cv_item_category.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CategoryActivity ma = new CategoryActivity();
                ma.popupCrudCategory(data.getCategoryID(), data.getCategoryName(), activity);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryEntities.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtCategoryName;
        protected CardView cv_item_category;

        public MyViewHolder (View view) {
            super(view);
            txtCategoryName = (TextView)view.findViewById(R.id.txtCategoryName);
            cv_item_category = (CardView) view.findViewById(R.id.cv_item_category);
        }
    }



}
